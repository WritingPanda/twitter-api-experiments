from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    DateTime,
    Boolean,
    ForeignKey,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import datetime


Base = declarative_base()


class Tweet(Base):
    __tablename__ = "tweets"
    id = Column(Integer, primary_key=True, unique=True, nullable=False)
    user_name = Column(String(50))
    screen_name = Column(String(50))
    user_id = Column(String(50))
    tweet_id = Column(String(50), unique=True)
    tweet_url = Column(String(255))
    tweet_text = Column(Text)
    tweet_text_html = Column(Text)
    tweet_timestamp = Column(DateTime, default=datetime.datetime.now())
    links = relationship("Link", backref="links")

    def __repr__(self):
        sn = self.screen_name
        tt = self.tweet_text
        return f"<Tweet (screen_name={sn}, tweet_text={tt})>"


class Link(Base):
    __tablename__ = "links"
    id = Column(Integer, primary_key=True, unique=True, nullable=False)
    link_url = Column(String(255))
    tweet_id = Column(Integer, ForeignKey("tweets.id"), nullable=False)
    link_has_processed = Column(Boolean, default=False)
    link_exists = Column(Boolean, default=False)

    def __repr__(self):
        tweet = self.tweet_id
        link = self.link_url
        return f"<Link (url={link}, tweet_id={tweet}>"
