from twitter_api.data.models import Base
from twitter_api.data.utils import get_db_engine_sqlite


def create_db() -> None:
    """
    Creates a database using the models as described in models.py.
    :return:
    """
    engine = get_db_engine_sqlite()
    Base.metadata.create_all(engine)

# TODO: Look up ways to migrate changes to DB using SQLAlchemy
