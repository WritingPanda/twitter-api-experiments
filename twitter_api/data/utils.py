from pathlib import Path
from decouple import config
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.engine.base import Engine
from twitter_api.data.models import Tweet
import tweepy


def get_db_engine_sqlite() -> Engine:
    parent_directory = Path(__file__).parent.absolute().parent.absolute()
    db_name = "db.sqlite3"
    db_path = f"sqlite:///{parent_directory.joinpath(db_name)}"
    if config("ENV") == "DEBUG":
        engine = create_engine(db_path, echo=True)
    else:
        engine = create_engine(db_path)
    return engine


def get_session() -> Session:
    session_obj = sessionmaker()
    session = session_obj(bind=get_db_engine_sqlite())
    return session


def set_auth():
    consumer_key = config("CONSUMER_KEY")
    consumer_secret = config("CONSUMER_SECRET")
    access_token = config("ACCESS_TOKEN")
    access_secret = config("ACCESS_SECRET")

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(
        auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True
    )
    return api


def does_tweet_exist_in_db(tweet_obj: Tweet, session: Session) -> bool:
    tweet = session.query(Tweet).filter_by(tweet_id=tweet_obj.tweet_id).all()
    if tweet:
        return True
    else:
        return False


def save_to_db(tweet: Tweet, session: Session, links: list = None) -> None:
    if links:
        tweet.links = links
    session.add(tweet)
    session.commit()
