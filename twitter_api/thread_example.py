import threading
import concurrent.futures
import random
import logging
import time
import queue


def producer(queue: queue.Queue, event: threading.Event):
    while not event.is_set():
        message = random.randint(1, 101)
        logging.info(f"Producer got message: {message}")
        queue.put(message)
        time.sleep(0.2)

    logging.info("Producer received EXIT event. Exiting.")


def consumer(queue: queue.Queue, event: threading.Event):
    while not event.is_set() or not queue.empty():
        message = queue.get()
        logging.info(
            f"Consumer storing message: {message} (queue size="
            f"{queue.qsize()})"
        )
        time.sleep(1)
    logging.info("Consumer received EXIT event. Exiting")


if __name__ == "__main__":
    log_format = "%(asctime)s: %(message)s"
    logging.basicConfig(
        format=log_format, level=logging.INFO, datefmt="%H:%M:%S"
    )
    # logging.getLogger().setLevel(logging.DEBUG)

    pipeline = queue.Queue(maxsize=10)
    event = threading.Event()
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        executor.submit(producer, pipeline, event)
        executor.submit(consumer, pipeline, event)

        time.sleep(60)
        logging.info("Main: about to set event")
        event.set()
