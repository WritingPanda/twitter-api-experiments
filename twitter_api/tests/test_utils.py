from twitter_api.data.utils import get_db_engine_sqlite, get_session, set_auth
from twitter_api.data.create_db import create_db
from pathlib import Path
import sqlalchemy


def test_get_db_engine_sqlite_type():
    engine = get_db_engine_sqlite()
    assert type(engine) is sqlalchemy.engine.base.Engine


def test_get_db_engine_sqlite_path():
    engine = get_db_engine_sqlite()
    parent_directory = Path(__file__).parent.absolute().parent.absolute()
    db_name = "db.sqlite3"
    db_path = f"sqlite:///{parent_directory.joinpath(db_name)}"
    assert str(engine.url) == db_path


def test_get_db_engine_sqlite_dialect():
    engine = get_db_engine_sqlite()
    assert (
        type(engine.dialect)
        == sqlalchemy.dialects.sqlite.pysqlite.SQLiteDialect_pysqlite
    )


def test_get_session():
    session = get_session()
    assert type(session) is not None
    assert session.is_active is True


def test_set_auth():
    api = set_auth()
    assert api.auth is not None
    assert api.get_user("Cisco").screen_name == "Cisco"


def test_create_db_if_database_file_exists():
    create_db()
    parent_directory = Path(__file__).parent.absolute().parent.absolute()
    db_name = "db.sqlite3"
    db_path = f"{parent_directory.joinpath(db_name)}"
    assert Path(db_path).exists() is True
