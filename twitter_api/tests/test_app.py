import datetime
import validators
from twitter_api.app import (
    get_tweets_from_keyword,
    create_tweet_object,
    create_link_object,
)
from twitter_api.data.models import Link
from twitter_api.utils.link_utils import (
    unshorten_url,
    is_twitter_url,
    does_raw_tweet_have_links,
    clean_link_before_creating_object,
)
from twitterscraper import tweet


def get_example_tweets() -> list:
    tweets = get_tweets_from_keyword(keyword="infosec")
    return tweets


def get_single_tweet_with_link(tweets: list) -> tweet:
    raw_tweet = None
    for test_tweet in tweets:
        if does_raw_tweet_have_links(test_tweet):
            raw_tweet = test_tweet
            break
    return raw_tweet


def get_link_from_tweet(raw_tweet: tweet) -> str:
    for link in raw_tweet.links:
        return link


example_tweets = get_example_tweets()


def test_does_raw_tweet_have_links():
    raw_tweet = None
    for test_tweet in example_tweets:
        if does_raw_tweet_have_links(test_tweet):
            raw_tweet = test_tweet
            break
    assert raw_tweet is not None
    assert type(raw_tweet)


def test_get_tweets_from_hashtag():
    tweets = example_tweets
    assert tweets is not None
    assert type(tweets) is list


def test_create_tweet_object():
    tweet_example = example_tweets[0]
    tweet_obj = create_tweet_object(tweet_example)
    assert tweet_obj.user_name == tweet_example.username
    assert tweet_obj.tweet_text == tweet_example.text
    assert tweet_obj.tweet_timestamp == tweet_example.timestamp
    assert type(tweet_obj.tweet_timestamp) is datetime.datetime


def test_create_link_object():
    raw_tweet = get_single_tweet_with_link(example_tweets)
    assert raw_tweet is not None
    tweet_obj = create_tweet_object(raw_tweet)
    link_obj_list = create_link_object(
        raw_tweet=raw_tweet, tweet_obj=tweet_obj
    )
    assert type(link_obj_list) is list
    assert type(link_obj_list[0]) is Link
    assert len(link_obj_list) > 0


def test_unshorten_url():
    raw_tweet_with_links = get_single_tweet_with_link(example_tweets)
    assert raw_tweet_with_links is not None
    assert type(raw_tweet_with_links.links) is list
    link = get_link_from_tweet(raw_tweet_with_links)
    final_url = unshorten_url(link)
    assert final_url is not None
    assert validators.url(final_url) is True


def test_is_twitter_url():
    twitter_url = "https://twitter.com"
    www_twitter_url = "https://www.twitter.com"
    result = is_twitter_url(url=twitter_url)
    result2 = is_twitter_url(url=www_twitter_url)
    assert result is True
    assert result2 is True
    not_twitter_url = "https://google.com"
    second_result = is_twitter_url(url=not_twitter_url)
    assert second_result is False
    invalid_url = "hokeypokey"
    third_result = is_twitter_url(url=invalid_url)
    assert third_result is False


def test_clean_link_before_creating_object():
    raw_tweet = get_single_tweet_with_link(example_tweets)
    assert raw_tweet is not None
    assert type(raw_tweet.links) is list
    link = get_link_from_tweet(raw_tweet)
    assert link is not None
    clean_url = clean_link_before_creating_object(link)
    assert clean_url is not None
    assert validators.url(clean_url) is True
