from twitter_api.tweet import (
    create_tweet_object,
    is_link_new,
    create_links,
    does_link_exist,
    is_retweet,
)
from twitter_api.data import Tweet
from twitter_api.data.utils import set_auth, get_session
import tweepy


def get_example_tweet() -> tweepy.Status:
    api = set_auth()
    for status in tweepy.Cursor(
        api.user_timeline, id="WrittenByAPanda", tweet_mode="extended"
    ).items():
        if status.id_str == "1231632505045946369":
            return status


example_tweet_status_obj = get_example_tweet()


def create_test_tweet_object() -> Tweet:
    new_tweet = create_tweet_object(tweet_status=example_tweet_status_obj)
    return new_tweet


def create_link_objs() -> list:
    links = example_tweet_status_obj.entities.get("urls")
    tweet = create_test_tweet_object()
    session = get_session()
    link_obj_list = create_links(links=links, tweet=tweet, session=session)
    return link_obj_list


example_tweet_obj = create_test_tweet_object()
example_link_obj_list = create_link_objs()


def test_create_tweet():
    base_url = "https://www.twitter.com"
    screen_name = example_tweet_status_obj.user.screen_name
    tweet_id = example_tweet_status_obj.id_str
    test_url = f"{base_url}/{screen_name}/status/{tweet_id}"

    tweet = create_tweet_object(example_tweet_status_obj)
    assert type(tweet) is Tweet
    assert tweet.tweet_url == test_url
    assert tweet.user_name == example_tweet_status_obj.user.name
    assert tweet.screen_name == example_tweet_status_obj.user.screen_name
    assert tweet.tweet_text == example_tweet_status_obj.full_text


def test_is_link_new():
    session = get_session()
    test_links = example_tweet_status_obj.entities.get("urls")
    expanded_links = [link.get("expanded_url") for link in test_links]
    for link in expanded_links:
        new_link = is_link_new(link=link, session=session)
        assert new_link is True


def test_create_links():
    assert example_link_obj_list is not None
    assert len(example_link_obj_list) == len(
        example_tweet_status_obj.entities.get("urls")
    )
    assert example_link_obj_list[0].link_url is not None


def test_does_link_exist():
    link_obj = example_link_obj_list[0]
    result = does_link_exist(link_obj)
    assert result is True


def test_is_retweet():
    tweet_text = example_tweet_obj.tweet_text
    result = is_retweet(tweet_text)
    assert result is False
    assert tweet_text[:2] != "RT "
