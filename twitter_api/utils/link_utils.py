import requests
import validators
from twitterscraper import tweet
from urllib.parse import urlparse


def unshorten_url(url_string: str) -> str:
    """
    Take a URL and redirect to the final location.
    Return the final location.
    Taken from Stack Overflow:
    https://stackoverflow.com/questions/7153096/how-can-i-un-shorten-a-url-using-python/7153185#7153185
    :param url_string:
    :return:
    """
    try:
        final_url_location = requests.head(
            url_string, allow_redirects=True
        ).url
        return final_url_location
    except ConnectionError:
        pass
    except requests.exceptions.SSLError:
        pass


def is_twitter_url(url: str) -> bool:
    """
    Take in a URL and compare its domain to twitter.com.
    If it is equal, return True.
    If is not not equal, return False.
    :param url: A string URL.
    :return boolean: The result of the comparison of twitter.com
    and the provided URL.
    """
    twitter_url = urlparse("https://twitter.com").netloc
    www_twitter_url = urlparse("https://www.twitter.com").netloc
    url_location = urlparse(url).netloc
    result = twitter_url == url_location or www_twitter_url == url_location
    return result


def does_raw_tweet_have_links(raw_tweet: tweet) -> bool:
    """
    Check if raw tweet has links.
    We only care about tweets with links.
    :param raw_tweet:
    :return:
    """
    if raw_tweet.links or len(raw_tweet.links) > 0:
        for link in raw_tweet.links:
            if not is_twitter_url(link):
                return True
    else:
        return False


def clean_link_before_creating_object(url: str) -> str or None:
    """
    Take the links from the raw tweet and clean them before
    creating a Link object.
    :param url:
    :return:
    """
    if validators.url(url):
        if not is_twitter_url(url):
            clean_url = unshorten_url(url)
            return clean_url
