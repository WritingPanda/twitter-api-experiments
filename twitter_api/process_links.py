from .data import Link
from .data.utils import get_session
from sqlalchemy.orm import Session
import logging
import time


logging.basicConfig(level=logging.DEBUG)


def get_links(session: Session) -> list:
    links = session.query(Link).filter_by(link_has_processed=False).all()
    return links


def process_link(link_url_str: str, session: Session) -> None:
    """
    Sample function to test updating the DB with link that has been "processed"
    :param session:
    :param link_url_str:
    :return:
    """
    link_obj = session.query(Link).filter(Link.link_url == link_url_str)
    if link_obj:
        link_obj.update({"link_has_processed": True})
        session.commit()


def main():
    session = get_session()
    while True:
        links = get_links(session=session)
        if links:
            for link in links:
                process_link(session=session, link_url_str=link.link_url)
        else:
            logging.debug("No links.")
        time.sleep(30)


if __name__ == "__main__":
    main()
