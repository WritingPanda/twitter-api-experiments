from twitter_api.data import Tweet, Link
from twitter_api.data.create_db import create_db
from twitter_api.data.utils import get_session, set_auth, save_to_db
from decouple import config
from pathlib import Path
from sqlalchemy.orm import Session
import logging
import requests
import tweepy


logging.basicConfig(level=logging.DEBUG)


def is_retweet(tweet_text: str) -> bool:
    """
    Check if a tweet is a retweet.
    :param tweet_text:
    :return boolean:
    """
    if tweet_text[:2] == "RT ":
        return True
    else:
        return False


def create_tweet_object(tweet_status: tweepy.models.Status) -> Tweet:
    """
    Create Tweet object.

    :param tweet_status:
    :return Tweet:
    """
    base_url = "https://www.twitter.com"
    user_name = tweet_status.user.name
    user_id = tweet_status.user.id_str
    screen_name = tweet_status.user.screen_name
    tweet_id = tweet_status.id_str
    tweet_url = f"{base_url}/{screen_name}/status/{tweet_id}"
    tweet_timestamp = tweet_status.created_at
    try:
        tweet_text = tweet_status.full_text
    except AttributeError:
        tweet_text = tweet_status.text

    new_tweet = Tweet(
        user_name=user_name,
        screen_name=screen_name,
        user_id=user_id,
        tweet_id=tweet_id,
        tweet_url=tweet_url,
        tweet_text=tweet_text,
        tweet_text_html=tweet_text,
        tweet_timestamp=tweet_timestamp,
    )
    return new_tweet


def is_link_new(link: str, session: Session) -> bool:
    link = session.query(Link).filter_by(link_url=link).all()
    if link:
        return False
    else:
        return True


def does_link_exist(link_obj: Link) -> bool:
    req = requests.get(link_obj.link_url)
    if req.status_code == 200:
        return True
    elif req.status_code >= 300:
        return False


def create_links(links: list, tweet: Tweet, session: Session) -> list:
    link_obj_list = list()
    for link in links:
        if "expanded_url" in link:
            expanded_url = link.get("expanded_url")
            # Keeping self-referential tweets out of the DB
            if "https://twitter.com/" not in expanded_url:
                if is_link_new(expanded_url, session):
                    new_link = Link(
                        link_url=link.get("expanded_url"), tweet_id=tweet.id
                    )
                    new_link.link_exists = does_link_exist(new_link)
                    link_obj_list.append(new_link)
    return link_obj_list


class MyStreamListener(tweepy.StreamListener):
    def __init__(self):
        super().__init__()
        self.session = get_session()

    def on_status(self, status):
        if not is_retweet(status.text):
            new_tweet = create_tweet_object(tweet_status=status)
            # Only save tweet if there are links we can use
            if status.entities.get("urls"):
                link_obj_list = create_links(
                    links=status.entities.get("urls"),
                    tweet=new_tweet,
                    session=self.session,
                )
                save_to_db(
                    tweet=new_tweet, session=self.session, links=link_obj_list,
                )

    def on_error(self, status_code):
        if status_code == 420:
            return False


def main():
    if config("ENV") == "DEBUG":
        db_path = Path(__file__).parent.absolute().joinpath("db.sqlite3")
        if not db_path.exists():
            create_db()

    api = set_auth()

    keyword_list = [
        "infosec",
        "exploit",
        "hack",
        "threat intel",
        "hacker",
    ]

    stream_listener = MyStreamListener()
    stream = tweepy.Stream(
        auth=api.auth, listener=stream_listener, tweet_mode="extended"
    )
    stream.filter(track=keyword_list, is_async=True)


if __name__ == "__main__":
    main()
