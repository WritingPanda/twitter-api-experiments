import logging
from datetime import date, timedelta
from decouple import config
from pathlib import Path
from twitter_api.data.create_db import create_db
from twitter_api.data.models import Tweet, Link
from twitter_api.data.utils import (
    get_session,
    save_to_db,
    does_tweet_exist_in_db,
)
from twitter_api.utils.link_utils import (
    clean_link_before_creating_object,
    does_raw_tweet_have_links,
)
from twitterscraper.query import query_tweets
from twitterscraper import tweet


logging.basicConfig(level=logging.DEBUG)


def get_tweets_from_keyword(
    keyword: str,
    limit: int = 100,
    begin_date: date = date.today() - timedelta(days=1),
    end_date: date = date.today(),
) -> list:
    """
    Scrape tweets bashed on the keyword, limit, and begin
    and end dates provided.
    :param keyword:
    :param limit:
    :param begin_date:
    :param end_date:
    :return:
    """
    tweets = query_tweets(
        keyword, limit=limit, begindate=begin_date, enddate=end_date
    )
    return tweets


def create_tweet_object(raw_tweet: tweet) -> Tweet:
    """
    Create tweet object from models.
    :param raw_tweet:
    :return:
    """
    new_tweet_obj = Tweet(
        user_name=raw_tweet.username,
        screen_name=raw_tweet.screen_name,
        user_id=raw_tweet.user_id,
        tweet_id=raw_tweet.tweet_id,
        tweet_url=raw_tweet.tweet_url,
        tweet_text=raw_tweet.text,
        tweet_text_html=raw_tweet.text_html,
        tweet_timestamp=raw_tweet.timestamp,
    )
    return new_tweet_obj


def create_link_object(raw_tweet: tweet, tweet_obj: Tweet) -> list:
    """
    Create a link object from models.
    :param raw_tweet:
    :param tweet_obj:
    :return:
    """
    tweet_links = raw_tweet.links
    link_obj_list = list()
    for link in tweet_links:
        clean_link = clean_link_before_creating_object(link)
        if clean_link is not None:
            new_link_obj = Link(link_url=clean_link, tweet_id=tweet_obj.id,)
            link_obj_list.append(new_link_obj)
            return link_obj_list


def main() -> None:
    if config("ENV") == "DEBUG":
        db_path = Path(__file__).parent.absolute().joinpath("db.sqlite3")
        if not db_path.exists():
            create_db()

    session = get_session()
    raw_tweets = get_tweets_from_keyword(keyword="infosec")
    for raw_tweet in raw_tweets:
        if does_raw_tweet_have_links(raw_tweet=raw_tweet):
            new_tweet = create_tweet_object(raw_tweet=raw_tweet)
            link_obj_list = create_link_object(
                raw_tweet=raw_tweet, tweet_obj=new_tweet
            )
            if not does_tweet_exist_in_db(
                tweet_obj=new_tweet, session=session
            ):
                save_to_db(
                    tweet=new_tweet, session=session, links=link_obj_list
                )


if __name__ == "__main__":
    main()
